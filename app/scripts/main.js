$(document).ready(function () {

  $('.phone').inputmask({
    mask: '+7(999)-999-99-99',
    showMaskOnHover: false
  });

  $('.a-header__toggle').on('click', function (e) {
    e.preventDefault();

    $('.a-header__menu').slideToggle('fast');
  });

  $('.a-home-seo__toggle').on('click', function (e) {
    e.preventDefault();

    $(this).toggleClass('a-home-seo__toggle_active');
    $(this).prev().toggleClass('a-home-seo__content_active');
  });

  $('.open-modal').on('click', function (e) {
    e.preventDefault();

    $('.a-modal').toggle();
  });

  $('.a-modal__centered').on('click', function (e) {
    e.preventDefault();

    if (e.target.className === 'a-modal__centered') {
      $('.a-modal').hide();
    }
  });

  $('.a-modal__close').on('click', function (e) {
    e.preventDefault();
    $('.a-modal').hide();
  });

  $('.a-aside-collapse__header').on('click', function (e) {
    e.preventDefault();
    $(this).toggleClass('a-aside-collapse__header_active');
    $(this).next().slideToggle('fast');
  });

  $('.a-tabs__header li').click(function () {
    var tab_id = $(this).attr('data-tab');

    $('.a-tabs__header li').removeClass('current');
    $('.a-tabs__content').removeClass('current');

    $(this).addClass('current');
    $('#' + tab_id).addClass('current');
  });

  $('.a-switch__header li').click(function () {
    var tab_id = $(this).attr('data-tab');

    $('.a-switch__header li').removeClass('current');
    $('.a-switch__content').removeClass('current');

    $(this).addClass('current');
    $('#' + tab_id).addClass('current');
  });

  $('.a-order__button_2').on('click', function () {
    $('#switch-2').trigger('click');
  });

  $('.a-order__button_1').on('click', function () {
    $('#switch-1').trigger('click');
  });

  $('.a-order__button_3').on('click', function () {
    $('#switch-3').trigger('click');
  });

  $('.a-basket-card__minus').on('click', function (e) {
    e.preventDefault();

    var currentVal = $(this).next().val();

    if (currentVal > 1) {
      $(this).next().val(--currentVal);
    }
  });

  $('.a-basket-card__plus').on('click', function (e) {
    e.preventDefault();

    var currentVal = $(this).prev().val();

    if (currentVal < 99) {
      $(this).prev().val(++currentVal);
    }
  });

  $('.a-home-cards-card__like').on('click', function(e) {
    e.preventDefault();

    $(this).toggleClass('a-home-cards-card__like_active');
  });

  $(window).on('load resize', function () {

    if ($(window).width() < 1200) {
      $('.a-header__nav').detach().appendTo('.a-header__menu');
      $('.a-header__search').detach().appendTo('.a-header__menu');
    }
  });

  new Swiper('.a-home-top', {
    loop: true,
    pagination: {
      el: '.a-home-top .swiper-pagination',
      clickable: true,
    },
    autoplay: {
      delay: 5000,
    },
  });

  new Swiper('.a-home-set__cards', {
    navigation: {
      nextEl: '.a-home-set__cards .swiper-button-next',
      prevEl: '.a-home-set__cards .swiper-button-prev',
    },
    loop: true,
    spaceBetween: 30,
    slidesPerView: 1,
    breakpoints: {
      1024: {
        slidesPerView: 2,
      },
      1200: {
        slidesPerView: 3,
      },
    }
  });

  new Swiper('.a-home-news__cards', {
    navigation: {
      nextEl: '.a-home-news__cards .swiper-button-next',
      prevEl: '.a-home-news__cards .swiper-button-prev',
    },
    loop: true,
    spaceBetween: 30,
    slidesPerView: 1,
    breakpoints: {
      1024: {
        slidesPerView: 2,
      },
      1200: {
        slidesPerView: 3,
      },
    }
  });

  new Swiper('#anotherCards', {
    navigation: {
      nextEl: '#anotherCards .swiper-button-next',
      prevEl: '#anotherCards .swiper-button-prev',
    },
    loop: true,
    spaceBetween: 30,
    slidesPerView: 1,
    breakpoints: {
      768: {
        slidesPerView: 2,
      },
      1024: {
        slidesPerView: 3,
      },
      1200: {
        slidesPerView: 4,
      },
    }
  });

  new Swiper('#similarCards', {
    navigation: {
      nextEl: '#similarCards .swiper-button-next',
      prevEl: '#similarCards .swiper-button-prev',
    },
    loop: true,
    spaceBetween: 30,
    slidesPerView: 1,
    breakpoints: {
      768: {
        slidesPerView: 2,
      },
      1024: {
        slidesPerView: 3,
      },
      1200: {
        slidesPerView: 4,
      },
    }
  });

  var galleryThumbs = new Swiper('.gallery-thumbs', {
    direction: 'vertical',
    spaceBetween: 5,
    slidesPerView: 'auto',
    freeMode: true,
    watchSlidesVisibility: true,
    watchSlidesProgress: true,
    breakpoints: {
      1200: {
        spaceBetween: 17,
        slidesPerView: 6,
      },
    }
  });
  var galleryTop = new Swiper('.gallery-top', {
    spaceBetween: 10,
    thumbs: {
      swiper: galleryThumbs
    }
  });

  let slider = document.getElementById('price');

  noUiSlider.create(slider, {
    start: [0, 20000],
    connect: true,
    step: 10,
    range: {
      'min': 0,
      'max': 50000
    }
  });

  slider.noUiSlider.on('update', function (values, handle) {

    $('#sort_min').html(Math.round(values[0]));
    $('#sort_max').html(Math.round(values[1]));
  });












  $('.w-basket-card__minus').on('click', function (e) {
    e.preventDefault();

    var currentVal = $(this).next().val();

    if (currentVal > 1) {
      $(this).next().val(--currentVal);
    }
  });

  $('.w-basket-card__plus').on('click', function (e) {
    e.preventDefault();

    var currentVal = $(this).prev().val();

    if (currentVal < 99) {
      $(this).prev().val(++currentVal);
    }
  });

  new Swiper('.w-welcome', {
    pagination: {
      el: '.w-welcome .swiper-pagination',
      clickable: true,
    }, autoplay: {
      delay: 5000,
    },
  });

  new Swiper('.w-good__images', {
    navigation: {
      nextEl: '.w-good__images .swiper-button-next',
      prevEl: '.w-good__images .swiper-button-prev',
    },
    pagination: {
      el: '.w-good__images .swiper-pagination',
      clickable: true,
    },
    breakpoints: {
      1200: {
        slidesPerView: 2,
      },
    }
  });
});